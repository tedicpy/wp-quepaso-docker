ARG WPVERSION
FROM wordpress:${WPVERSION:-latest}
LABEL maintainer="Lu Pa <admin@tedic.org>"

# Instalo wp-cli
RUN curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar \
  && chmod +x wp-cli.phar \
  && mv wp-cli.phar /usr/local/bin/wp \
  && mkdir -p /var/www/.wp-cli/cache \
  && chmod 777 /var/www/.wp-cli/cache

# Para colocar conf de php especificas
ADD customphp.ini /usr/local/etc/php/conf.d/

WORKDIR /var/www/html
